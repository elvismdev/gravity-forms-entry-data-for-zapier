<?php
/**
 * Plugin Name: GCTV Gravity Forms Entry Data For Zapier
 * Plugin URI: https://bitbucket.org/grantcardone/gctv-gravity-forms-entry-data-for-zapier
 * Description: WP plugin filter to override Gravity Forms default hidden field values by entry meta values needed to be sent to Zapier. Like Entry ID and Transaction ID.
 * Version: 1.0.0
 * Author: Elvis Morales
 * Author URI: http://elvismdev.io/
 * Requires at least: 3.5
 * Tested up to: 4.5.2
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_filter( 'gform_zapier_field_value', 'gctv_change_field_value', 10, 4 );
function gctv_change_field_value( $value, $form_id, $field_id, $entry ) {

	$fields = array( 
		11,	// Entry/Order ID
		12,	// Transaction ID
		13,	// Webcast Name
		14,	// Webcast Price
		15	// Webcast QTY
		);

	if ( in_array( $field_id, $fields ) && ! empty( $value ) ) {
		$value = rgar( $entry, $value );
	}

	return $value;

}